# Docker Repo on Nexus 

Create Docker repository on Nexus and push it 

## Table of Contents
- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Tests](#test)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Create Docker hosted repository on Nexus

* Create Docker repository role on Nexus

* Configure Nexus, DigitalOcean Droplet and Docker to be able to push to Docker repository

* Build and Push Docker image to Docker repository on Nexus

## Technologies Used

* Docker 

* Nexus 

* DigitalOcean 

* Linux   

## Steps

Step 1: Create a Cloud server on DigitalOcean

[Cloud Server](/images/01_cloud_server.png)

Step 2: Configure Network Firewall on DigitalOcean to allow ssh listen on port 22 

[Cloud Server Firewall](/images/02_cloud_server_firewall_ports.png)

Step 3: Ssh into server from client 

    ssh root@138.68.156.82 

[Ssh to Server](/images/03_login_into_server.png)

Step 4: Create a nexus user on server and create authorized key file in the .ssh directory 

    adduser nexus
    mkdir /home/nexus/.ssh
    vim /home/nexus/.ssh/authorized_keys 

[Nexus User & A keys file](/images/04_created_nexus_user_and_added_authorized_keys.png)

Step 5: Using an editor like vim input client public ssh key into authorized keys and save and quit with :wq!

Step 6: logout and try and login into cloud server through the nexus user (Best practice for security reasons)

    ssh nexus@138.68.156.82 

[Login with nexus user](/images/05_login_into_cloud_nexus_user.png)

Step 7: Install java 8 for nexus and net-tools to be able to access netstat to check active ports 

    apt install openjdk-8-jre-headless net-tools 

[install java 8 and net tools](/images/06_installing_java_and_net_tools.png)

Step 8: Download nexus on cloud server 

    wget -c https://download.sonatype.com/nexus/3/nexus-3.42.0-01-unix.tar.gz

[Download nexus on cloud](/images/07_downoading_nexus.png)

Step 9: Extract downloaded nexus tar file

    tar -xzvf nexus-3.42.0-01-unix.tar.gz

[Extracting nexus file](/images/08_extracting_nexus.png)

Step 10: Change nexus files ownership to the nexus User

    chown -R nexus:nexus nexus-3.42.0-01 sonatype-work

[Change Ownership](/images/09_changing_nexus_ownership.png)

Step 11: configure nexus.rc to enable nexus user to run nexus 

    vim nexus.rc  

[nexus.rc config](/images/10_configuring_nexus_rc.png)

Step 12: start nexus 

    /opt/nexus-3.42.0-01/bin/nexus start

[Start Nexus](/images/10_starting_nexus.png)

Step 13: check the port in which nexus is listening 

    netstat -lpnt

[Active nexus port](/images/11_port_nexus_is_listening.png)

Step 14: Check nexus UI in your browser localhost:8081

[Nexus UI](/images/12_nexus_UI.png)

Step 15: Create a store on nexus 

[Nexus Store](/images/13_created_store.png)

Step 16: Create a docker hosted repo on nexus 

[Create Docker Hosted](/images/14_docker_hosted_repo_created.png)


Step 17: Create a role on nexus repo for to enable privileges for docker hosted

[Created Nexus Role](/images/15_creating_role.png)

Step 18: Create a nexus user who will have privileges of the docker hosted role 

[Created Nexus User](/images/10_creating_user.png)

Step 19: Enabling docker hosted token for authentication when login into docker hosted

[Docker Bearer Token](/images/16_active_docker_bearer_token.png)


Step 20: Adding different port for docker hosted because port 8081 is already occupied by nexus 

[Different Port](/images/17_adding_a_different_port_for_docker_repo.png)

Step 21: Add insecure registries on Docker Desktop because docker hosted is not secured 

[Added Insecure registries](/images/18_adding_insecure_registries.png)

Step 22: Log into nexus repo 

    docker login 136.68.156.82:8083

[login Nexus Repo](/images/19_login_into_nexus_repo_dh.png)

Step 23: Build image you want to push 

    docker build -t my-image:1.0 .. 

[Built Docker Image](/images/20_build_image.png)

Step 24: Re-tag built image in step 23 

    docker tag my-image:1.0 138.68.156.82:8083/my-image:1.0

[Retagging Image](/images/21_retagging_the_image.png)

Step 25: Push image to nexus repo 

    docker push 138.68.156.82:8083/my-image:1.0

[Push Image](/images/22_pushing_image_to_nexus_repo.png)

Step 26: Check image in docker hosted nexus repo 

[Check Image](/images/23_image_on_docker_hosted.png)

Step 27: Fetch image from nexus docker hosted repo 

    curl -u oma:test -X GET 'http://138.68.156.82:8081/service/rest/v1/components?'repository=docker-hosted

[Fetch Image](images/24_fetching_docker_image_from_nexus.png)

## Installation

* apt install openjdk-8-jre-headless 

* apt install net-tools 

*  wget -c https://download.sonatype.com/nexus/3/nexus-3.42.0-01-unix.tar.gz

## Usage 

$ /opt/nexus-3.42.0-01/bin/nexus start

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/docker-repository-on-nexus.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review

## Tests

Test were ran using npm test.

## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/docker-repository-on-nexus

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.